package ru.domclick.hrbot.dao

import org.junit.jupiter.api.Test
import org.testng.Assert

class GetDataTest() {

    @Test
    fun getSpecificityList() {
        val query = GetData().getSpecificityList()
        Assert.assertEquals(query.size, 2)
    }
}