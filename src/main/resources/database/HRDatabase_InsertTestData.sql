--awareness
INSERT INTO awareness (id, name) VALUES ('0293df4e-5bfa-4205-a8ab-7ce0ee7eacc7','Хорошо знает о продукте/проекте');
INSERT INTO awareness (id, name) VALUES ('9bd70ebe-484e-45fd-be90-db610e7743e7','Слышал поверхностно');
INSERT INTO awareness (id, name) VALUES ('015b9891-1755-4f54-9c5a-cd66c8f5a34a','Не знает совсем');

--specificity
INSERT INTO specificity (id, name) VALUES ('ba998fa9-6baf-40a4-97b2-933bee03f9be','ИТ');
INSERT INTO specificity (id, name) VALUES ('07138991-98d5-4e95-a85d-0871dec127d3','Бизнес');


