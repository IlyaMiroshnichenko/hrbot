package ru.domclick.hrbot.exception

/**
 * Исключение, которое отбрасывается в случае, если пользователь ввел в чат сообщение вместо нажатия на кнопку
 */
class NeedChoiceException: Throwable() {

}