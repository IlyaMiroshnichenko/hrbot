package ru.domclick.hrbot.util

import ru.domclick.hrbot.entity.AwarenessEntity
import ru.domclick.hrbot.entity.SpecificityEntity
import java.util.*

/**
 * Общие утилиты
 */
class CommonUtils {

    /**
     * Вернуть Id значение по имени из справочника спецификаций
     */
    fun getIdFromSpecificityList(searchList: List<SpecificityEntity>, value: String): UUID {
        var result: UUID = UUID.randomUUID()
        for (item in searchList) {
            if (item.name == value) {
                result = item.id
                break
            }
        }
        return result
    }

    /**
     * Вернуть Id значение по имени из справочника осведомленности
     */
    fun getIdFromAwarenessList(searchList: List<AwarenessEntity>, value: String): UUID {
        var result: UUID = UUID.randomUUID()
        for (item in searchList) {
            if (item.name == value) {
                result = item.id
                break
            }
        }
        return result
    }
}