package ru.domclick.hrbot.util

import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration
import org.springframework.beans.factory.annotation.Autowired


/**
 * Класс, содержащий методы для работы с сессией Hibernate
 */
class HibernateUtil {

    @Autowired
    private val sessionFactory = buildSessionFactory()

    /**
     * Создать сессию Hibernate
     */
    private fun buildSessionFactory(): SessionFactory {
        try {
            return Configuration().configure("hibernate.cfg.xml").buildSessionFactory()
        } catch (ex: Throwable) {
            System.err.println("Initial SessionFactory creation failed.$ex")
            throw ExceptionInInitializerError(ex)
        }
    }

    /**
     * Открыть сессию Hibernate
     */
    fun openHibernateSession(): Session {
        return sessionFactory.openSession()
    }
}