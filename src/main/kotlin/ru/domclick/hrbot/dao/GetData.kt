package ru.domclick.hrbot.dao

import org.hibernate.Session
import ru.domclick.hrbot.entity.AwarenessEntity
import ru.domclick.hrbot.entity.SpecificityEntity
import ru.domclick.hrbot.util.HibernateUtil

/**
 * Класс, содержащий методы на чтение из базы данных
 */
class GetData {

    /**
     * Вернуть список возможных спецификаций кандидата
     */
    fun getSpecificityList(): MutableList<SpecificityEntity>  {
        val session: Session = HibernateUtil().openHibernateSession()
        var query: MutableList<SpecificityEntity> = mutableListOf()
        try {
            session.beginTransaction()
            query = session.createQuery("from ru.domclick.hrbot.entity.SpecificityEntity", SpecificityEntity::class.java).resultList
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            if (session.isOpen) {
                session.close()
            }
        }
        return query
    }

    /**
     * Вернуть список возможных осведомленностей кандидата
     */
    fun getAwarenessList(): MutableList<AwarenessEntity>  {
        val session: Session = HibernateUtil().openHibernateSession()
        var query: MutableList<AwarenessEntity> = mutableListOf()
        try {
            session.beginTransaction()
            query = session.createQuery("from ru.domclick.hrbot.entity.AwarenessEntity", AwarenessEntity::class.java).resultList
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            if (session.isOpen) {
                session.close()
            }
        }
        return query
    }

}