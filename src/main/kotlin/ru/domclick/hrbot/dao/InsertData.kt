package ru.domclick.hrbot.dao

import org.hibernate.Session
import ru.domclick.hrbot.entity.CandidateEntity
import ru.domclick.hrbot.util.HibernateUtil

/**
 * Класс, содержащий методы на вставку в базу данных
 */
class InsertData {

    fun insertCandidate(candidate: CandidateEntity): Boolean {
        var result = false
        var session: Session? = null
        try {
            session = HibernateUtil().openHibernateSession()
            session.beginTransaction()
            session.save(candidate)
            session.transaction.commit()
        } catch (e : Exception) {
            e.printStackTrace()
        } finally {
          if (session != null && session.isOpen) {
              session.close()
              result = true
          }
        }
        return result
    }
}