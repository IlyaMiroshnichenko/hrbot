package ru.domclick.hrbot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import ru.domclick.hrbot.config.BotConfig

@SpringBootApplication
class HrBotApplication

fun main(args: Array<String>) {
	BotConfig().initTelegramApi()
	runApplication<HrBotApplication>(*args)
}
