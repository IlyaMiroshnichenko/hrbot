package ru.domclick.hrbot.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import ru.domclick.hrbot.bot.Bot
import ru.domclick.hrbot.dto.Candidate
import ru.domclick.hrbot.dto.Process


/**
 * Конфигурация бота
 */
@Configuration
class BotConfig {

    /**
     * Инициализация телеграм бота
     */
    fun initTelegramApi() {
        ApiContextInitializer.init()
        val botApi = TelegramBotsApi()
        try {
            botApi.registerBot(Bot())
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Инициализация экземпляра класса Кандидат
     */
    @Bean
    fun getCandidate(): Candidate {
        return Candidate()
    }

    /**
     * Инициализация экземпляра класса Процесс
     */
    @Bean
    fun getProcess(): Process {
        return Process()
    }
}

