package ru.domclick.hrbot.services

import ru.domclick.hrbot.dao.InsertData
import ru.domclick.hrbot.entity.CandidateEntity

/**
 * Сервисы для работы с базой данных
 */
class CandidateService {

    /**
     * Сервис сохранения кандидата в базу данных
     */
    fun saveCandidate(candidate: CandidateEntity): String {
        val operationResult = InsertData().insertCandidate(candidate)
        val resultTxt: String
        resultTxt = if (operationResult) {
            "Кандидат успешно сохранен в базу данных!"
        } else {
            "Произошла ошибка при сохранении кандидата в базу данных!"
        }

        return resultTxt
    }
}