package ru.domclick.hrbot.dto

/**
 * Класс, предназначенный для хранения состояния процесса заполнения анкеты
 */
class Process {

    //Процесс заполнения начался
    var isStarted: Boolean = false

    //Необходимо сделать выбор (нажать на одну из кнопок)
    var needChoice: Boolean = false

    //ФИО кандидата заполнено
    var isFioFilled: Boolean = false

    //Кнопки выора спецификации показаны
    var isSpecificityButtonsShown: Boolean = false

    //Спецификация выбрана
    var isSpecificitySelected: Boolean = false

    //Кнопки осведомленности показаны
    var isAwarenessButtonsShown: Boolean = false

    //Осведомленность выбрана
    var isAwarenessSelected: Boolean = false

    //Кнопки выбора приглашения показаны
    var isInvitedButtonsShown: Boolean = false

    //Приглашение выбрано
    var isInvitedSelected: Boolean = false

}