package ru.domclick.hrbot.dto

/**
 * Класс, предназначенный для хранения информации о кандидате
 */
class Candidate {

    //ФИО кандидата
    var fio: String = ""

    //Спецификация кандидата
    var specificity: String = ""

    //Осведомленность кандидата
    var awareness: String = ""

    //Приглашение кандидата на собеседование
    var isInvited: Boolean = false

    //Принят ли кандидат на работу
    var isApplied: Boolean = false
}