package ru.domclick.hrbot.bot

import org.springframework.beans.factory.annotation.Autowired
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import ru.domclick.hrbot.dao.GetData
import ru.domclick.hrbot.dto.Candidate
import ru.domclick.hrbot.dto.Process
import ru.domclick.hrbot.entity.CandidateEntity
import ru.domclick.hrbot.exception.NeedChoiceException
import ru.domclick.hrbot.services.CandidateService
import ru.domclick.hrbot.util.CommonUtils
import java.util.*

/**
 * Базовые методы для работы с ботом
 */
class Bot: TelegramLongPollingBot() {

    @Autowired
    private var candidate = Candidate()

    @Autowired
    private var process = Process()

    @Autowired
    private val specificityList = GetData().getSpecificityList()

    @Autowired
    private val awarenessList = GetData().getAwarenessList()

    /**
     * Вернуть имя бота
     */
    override fun getBotUsername(): String {
        return "HrDomClickBot"
    }

    /**
     * Вернуть токен бота
     */
    override fun getBotToken(): String {
        return "1104888463:AAHoE7zZWWFJ6MfVdDzttyE9zWFc46rdhFg"
    }

    /**
     * Вернуть результат выполнения команды
     */
    override fun onUpdateReceived(upd: Update?) {
        if (upd!!.hasMessage() && upd.message.hasText()) {
            isCommand(upd.message.text, upd)
        }

        if(upd.hasCallbackQuery()) {
            try {
                process.needChoice = false
                execute(SendMessage().setText(upd.callbackQuery.data).setChatId(upd.callbackQuery.message.chatId))
                checkProcess(upd)
            } catch (e: TelegramApiException) {
                e.printStackTrace()
            }

        }
    }

    /**
     * Проверить, является ли введенная строка командой
     */
    private fun isCommand(command: String, upd: Update?) {
        when (command) {
            "/start" -> startProcess(upd)
            else -> checkProcess(upd)
        }
    }

    /**
     * Проверить, на какой стадии заполнения анкеты находится пользователь
     */
    private fun checkProcess(upd: Update?) {

        //Процесс запущен, ФИО не заполнено
        if (process.isStarted && !process.isFioFilled) {
            fillFio(upd)
        }

        //ФИО заполнено, кнопки выбора спецификации еще не отображены
        if (process.isFioFilled && !process.isSpecificityButtonsShown) {
            showSpecificityButtons(upd)
            process.needChoice = true
            return
        }

        //Кнопки выбора спецификации отображены, спецификация не выбрана
        if (process.isSpecificityButtonsShown && !process.isSpecificitySelected) {
            fillSpecificity(upd)
        }

        //Спецификация вырана, кнопки выбора осведомленности не показаны
        if (process.isSpecificitySelected && !process.isAwarenessButtonsShown) {
            showAwarenessButtons(upd)
            process.needChoice = true
            return
        }

        //Кнопки выбора осведомленности показаны, осведомленность не выбрана
        if (process.isAwarenessButtonsShown && !process.isAwarenessSelected) {
            fillAwareness(upd)
        }

        //Осведомленность выбрана, кнопки выбора приглашения не показаны
        if (process.isAwarenessSelected&& !process.isInvitedButtonsShown) {
            showIsInvitedButtons(upd)
            return
        }

        //Кнопки выбора приглашения показаны, приглашение не выбрано
        if (process.isInvitedButtonsShown && !process.isInvitedSelected) {
            fillInvited(upd)
        }
    }

    /**
     * Отправить сообщение в чат
     */
     private fun sendMessage(msg: Message, txt: String) {
        val sendMsg = SendMessage()
        sendMsg.setChatId(msg.chatId)
        sendMsg.text = txt
        try {
            execute(sendMsg)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Начать процесс заполнения анкеты
     */
    private fun startProcess(upd: Update?) {
        candidate = Candidate()
        process = Process()
        val msg = upd!!.message
        process.isStarted = true
        sendMessage(msg, "Начало процесса. Введите ФИО кандидата")
    }

    /**
     * Заполнить ФИО кандидата
     */
    private fun fillFio(upd: Update?) {
        val msg = upd!!.message
        val txt = msg.text
        candidate.fio = txt
        process.isFioFilled = true
    }

    /**
     * Заполнить спецификацию кандидата
     */
    private fun fillSpecificity(upd: Update?) {
        try {
            isCallbackQueryDataNull(upd?.callbackQuery)
            candidate.specificity = upd!!.callbackQuery.data
            process.isSpecificitySelected = true
        }
        catch (e: NeedChoiceException) {
            sendMessage(upd!!.message, "Необходимо выбрать спецификацию  кандидата")
        }
    }

    /**
     * Заполнить осведомленность кандидата
     */
    private fun fillAwareness(upd: Update?) {
        try {
            isCallbackQueryDataNull(upd?.callbackQuery)
            candidate.awareness = upd!!.callbackQuery.data
            process.isAwarenessSelected = true
        }
        catch (e: NeedChoiceException) {
            sendMessage(upd!!.message, "Необходимо выбрать осведомленность кандидата")
        }
    }

    /**
     * Заполнить приглашение кандидата
     */
    private fun fillInvited(upd: Update?) {
        try {
            isCallbackQueryDataNull(upd?.callbackQuery)
            candidate.isInvited = upd!!.callbackQuery.data == "Да"
            process.isInvitedSelected = true
            val result = CandidateService().saveCandidate(CandidateEntity(
                    UUID.randomUUID(),
                    candidate.fio,
                    CommonUtils().getIdFromSpecificityList(specificityList, candidate.specificity),
                    CommonUtils().getIdFromAwarenessList(awarenessList, candidate.awareness),
                    candidate.isInvited)
            )
            val sendMsg = SendMessage().setChatId(upd.callbackQuery.message.chatId).setText(result)
            execute(sendMsg)
            process.isSpecificityButtonsShown = true
        }
        catch (e: NeedChoiceException) {
            sendMessage(upd!!.message, "Необходимо выбрать был ли приглашен кандидат")
        }
        catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Показать кнопки выбора спецификации
     */
    private fun showSpecificityButtons(upd: Update?) {
        val inlineKeyboardMarkup = InlineKeyboardMarkup()

        val keyboardRow: MutableList<InlineKeyboardButton> = mutableListOf()
        for (spec in specificityList) {
            keyboardRow.add(InlineKeyboardButton().setText(spec.name).setCallbackData(spec.name))
        }

        val keyboardRows: MutableList<MutableList<InlineKeyboardButton>> = mutableListOf()
        keyboardRows.add(keyboardRow)

        inlineKeyboardMarkup.keyboard = keyboardRows

        val sendMsg = SendMessage().setChatId(upd!!.message.chatId.toString()).setText("Выберите спецификацию кандидата").setReplyMarkup(inlineKeyboardMarkup)

        try {
            execute(sendMsg)
            process.isSpecificityButtonsShown = true
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Показать кнопки выбора осведомленности
     */
    private fun showAwarenessButtons(upd: Update?) {
        val inlineKeyboardMarkup = InlineKeyboardMarkup()

        val keyboardRow: MutableList<InlineKeyboardButton> = mutableListOf()
        for (aware in awarenessList) {
            keyboardRow.add(InlineKeyboardButton().setText(aware.name).setCallbackData(aware.name))
        }

        val keyboardRows: MutableList<MutableList<InlineKeyboardButton>> = mutableListOf()
        keyboardRows.add(keyboardRow)

        inlineKeyboardMarkup.keyboard = keyboardRows

        val sendMsg = SendMessage().setChatId(upd!!.callbackQuery.message.chatId).setText("Выберите осведомленность кандидата").setReplyMarkup(inlineKeyboardMarkup)

        try {
            execute(sendMsg)
            process.isAwarenessButtonsShown = true
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Показать кнопки выбора приглашения
     */
    private fun showIsInvitedButtons(upd: Update?) {
        val inlineKeyboardMarkup = InlineKeyboardMarkup()

        val keyboardRow: MutableList<InlineKeyboardButton> = mutableListOf()
        keyboardRow.add(InlineKeyboardButton().setText("Да").setCallbackData("Да"))
        keyboardRow.add(InlineKeyboardButton().setText("Нет").setCallbackData("Нет"))

        val keyboardRows: MutableList<MutableList<InlineKeyboardButton>> = mutableListOf()
        keyboardRows.add(keyboardRow)

        inlineKeyboardMarkup.keyboard = keyboardRows

        val sendMsg = SendMessage().setChatId(upd!!.callbackQuery.message.chatId).setText("Кандидат приглашен на собеседование?").setReplyMarkup(inlineKeyboardMarkup)

        try {
            execute(sendMsg)
            process.isInvitedButtonsShown = true
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    /**
     * Проверка наличия ответного сообщения
     */
    private fun isCallbackQueryDataNull(callback: CallbackQuery?) {
         if (callback == null) {
             throw NeedChoiceException()
         }
    }
}