package ru.domclick.hrbot.entity

import java.util.*
import javax.persistence.*


/**
 * Сущность таблицы candidate
 */@Entity
@Table(name = "candidate")
class CandidateEntity(id: UUID = UUID.randomUUID(), name: String, idSpecificity: UUID, idAwareness: UUID, isInvited: Boolean) {

    @Id
    @Column(name = "id")
    var id: UUID = id

    @Column(name = "candidatename")
    var candidateName: String = name

    @Column(name = "idspecificity")
    var idSpecificity: UUID = idSpecificity

    @OneToOne
    @JoinColumn(name = "idspecificity", referencedColumnName = "id", insertable = false, updatable = false)
    var specificity: SpecificityEntity =  SpecificityEntity(UUID.randomUUID(),"")

    @Column(name = "idawareness")
    var idAwareness: UUID = idAwareness

    @OneToOne
    @JoinColumn(name = "idawareness", referencedColumnName = "id", insertable = false, updatable = false)
    var awareness: AwarenessEntity = AwarenessEntity(UUID.randomUUID(), "")

    @Column(name = "isinvited")
    var isInvited: Boolean = isInvited

    @Column(name = "isapplied")
    var isApplied: Boolean = false
}