package ru.domclick.hrbot.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Сущность таблицы awareness
 */
@Entity
@Table(name = "awareness")
class AwarenessEntity(id: UUID, name: String) {

    @Id
    @Column(name = "id")
    var id: UUID = id

    @Column(name = "name")
    var name: String = name
}