package ru.domclick.hrbot.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Сущность таблицы specificity
 */
@Entity
@Table(name = "specificity")
class SpecificityEntity(id: UUID, name: String) {

    @Id
    @Column(name = "id")
    var id: UUID = id

    @Column(name = "name")
    var name: String = name
}